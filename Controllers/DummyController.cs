﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebChat.Entities;

namespace WebChat.Controllers {
    [Route("api/[controller]")]
    public class DummyController : Controller {

        private WebChatAppContext _context;


        public DummyController(WebChatAppContext context) {
            this._context=context;
        }



        [HttpGet()]
        public IActionResult Test() {
            return Ok();
        }
    }
}