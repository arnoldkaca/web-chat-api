﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebChat.Entities;
using WebChat.Models;
using WebChat.Services;

namespace WebChat.Controllers {
    [Route("api/[controller]")]
    public class ProfileController : Controller {

        private IWebChatAppRepository _repository;

        public ProfileController(IWebChatAppRepository repository) {
            _repository=repository;
        }

        [HttpGet()]
        public IActionResult GetProfile() {
            var UserEntities = _repository.GetUsers();

            var results = new List<UserModel>();
            foreach (var userEntity in UserEntities) {
                results.Add(new UserModel {
                    Id=userEntity.Id,
                    FirstName=userEntity.FirstName,
                    LastName=userEntity.LastName,
                    Username=userEntity.Username,
                    Password=userEntity.Password,
                    Status=userEntity.Status,
                    RoleId=userEntity.RoleId,
                    IsActive=userEntity.IsActive,
                    ActiveCookie=userEntity.IsActive
                });
            }

            return Ok(results);
        }
        [HttpGet("{id}")]
        public IActionResult GetUserProfile( int id ) {
            var user = _repository.GetUser(id);
            if (user==null) {
                return NotFound("User not found!");
            }
            return Ok(user);
        }

        [HttpPost()]
        public IActionResult UpdateProfile( [FromBody] UserEntity user ) {
            if (user==null) {
                return BadRequest();
            }

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            
            

            return Ok(user);
        }
    }
}