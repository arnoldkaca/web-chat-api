﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebChat.Entities;
using WebChat.Models;
using WebChat.Services;

namespace WebChat.Controllers {
    [Route("api/[controller]")]
    public class RegisterController : Controller {

        private IWebChatAppRepository _repository;
        public RegisterController( IWebChatAppRepository repository ) {
            _repository=repository;
        }

    
        [HttpPost]
        public IActionResult RegisterUser([FromBody] UserModel User) {
            if(User == null) {
                return BadRequest("Please fill the data");
            }
            if (User.FirstName==null||User.LastName==null||User.Username==null)
                return BadRequest("Please Fill All the data!");

            if (User.FirstName.Length>50 || User.FirstName.Length<2)
                return BadRequest("First Name should contain less than 50 letters and more than 2");

            if (User.LastName.Length>50 || User.LastName.Length<2)
                return BadRequest("Last Name should contain less than 50 letters and more than 2");
            
            if(User.Username.Length>50 || User.Username.Length<8) 
                return BadRequest("Username should contain less than 50 letters and more than 8");

            if (@User.Status.Length>250)
                return BadRequest("Username should contain less than 250");

            if (_repository.UserExists(User.Username)) {
                return BadRequest("User Already Exists");
            }

            //Add Data to the Database
            //var usr = Mapper.Map<Entities.User>(User);

            var usr = new Entities.UserEntity {
                //from user
                FirstName=User.FirstName,       //Cannot be Null
                LastName=User.LastName,         //Cannot be Null
                Username=User.Username,         //Cannot be null
                Password=User.Password,         //Cannot be null
                Status=User.Status,             //can be null

                //Generated!
                RoleId=2,                   
                IsActive=1

            };

            _repository.AddUser(usr);
            //Save Data to the Database
            if (!_repository.SaveChanges()) {
                return StatusCode(500, "A problem happened while handling your request!");
            }

            return Ok("User \""+User.Username+"\" is registered successfully");
        }

        [HttpDelete]
        public IActionResult DeleteUser([FromBody] UserModel User) {
            

            return Ok();
        }
    }
}