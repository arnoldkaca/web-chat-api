﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebChat.Models;
using WebChat.Services;

namespace WebChat.Controllers {
    [Route("api/[controller]")]
    public class MessageController : Controller {
        private IWebChatAppRepository _rep;
        private int userId;

        public MessageController(IWebChatAppRepository rep) {
            _rep=rep;
        }
        [HttpGet()]
        public IActionResult GetMessages([FromBody] MessageModel msg) {
            //Check Authentication
            if (Request.Cookies["UserID"]==null) return NotFound("You are not Logged In");
            this.userId=int.Parse(Request.Cookies["UserID"]);
            var results = _rep.GetMessages(this.userId);
            if (results == null) {
                return NotFound("No Messages Found on the Database");    
            }

            return Ok(results);
        }

        [HttpPost()]
        public IActionResult SendMessage([FromBody] MessageModel msg) {
            if (Request.Cookies["UserID"]==null) return NotFound("You are not Logged In");
            this.userId=int.Parse(Request.Cookies["UserID"]);
            _rep.SendMessage(this.userId, msg.ReceiverId, msg.MessageText);
            return Ok();
        }
    }
}