﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebChat.Entities;
using WebChat.Services;

namespace WebChat.Controllers {
    [Route("api/[controller]")]
    public class LoginController : Controller {

        private IWebChatAppRepository _repostiry;

        private Random random = new Random();
        public string GenerateString( int length = 45 ) {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public LoginController( IWebChatAppRepository repostiry ) {
            _repostiry=repostiry;
        }

        [HttpGet()]
        public IActionResult GetLoggedIn() {
            if (Request.Cookies["UserID"]!=null||Request.Cookies["UID_auth"]!=null)
                if (!_repostiry.IsLoggedIn(int.Parse(Request.Cookies["UserID"]), Request.Cookies["UID_auth"])) 
                    return BadRequest("You are not logged in");


            return Ok("User Logged in!");
        }

        [HttpPost()]
        public IActionResult Login([FromBody] UserEntity User) {
            if (Request.Cookies["UserID"] != null ||Request.Cookies["UID_auth"] != null) 
                if (_repostiry.IsLoggedIn(int.Parse(Request.Cookies["UserID"]), Request.Cookies["UID_auth"])) 
                    return Ok("User Already Logged in!");
            
            CookieOptions cookie = new CookieOptions {
                Expires=DateTime.Now.AddDays(2)
            };
            var currentUIDAuth = GenerateString();
            Response.Cookies.Append("UID_auth", currentUIDAuth, cookie);
            var usr = _repostiry.LoginUser(User, currentUIDAuth);
            if (usr==null) {
                return NotFound("Credential mismatch!");
            }
            Response.Cookies.Append("UserID", usr.Id.ToString(), cookie);
            return Ok(usr);
            ////if (usr==null) {
            ////    return NotFound();
            ////}

            //return Ok(usr);
        }

        [HttpDelete()]
        public IActionResult Logout([FromBody] UserEntity User) {
            if (Request.Cookies["UserID"] !=null) {
                _repostiry.LogutUser(int.Parse(Request.Cookies["UserID"]));
                Response.Cookies.Append("UID_auth", "", new CookieOptions() { Expires=DateTime.Now });
                Response.Cookies.Append("UserID", "", new CookieOptions() { Expires=DateTime.Now });
                return Ok("User logged Out");
            }
            return Ok("No User Logged In!");
        }
    }
}