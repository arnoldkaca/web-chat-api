﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebChat.Entities;
using WebChat.Models;



namespace WebChat.Services {
    public class WebChatAppRepository : IWebChatAppRepository {

        private WebChatAppContext _context;
        
        public WebChatAppRepository(WebChatAppContext context) {
            _context=context;
        }

        //Add new User
        public void AddUser( UserEntity md ) {
            _context.Users.Add(md);
        }

        //Get Message -> UserID MessageID
        public MessageEntity GetMessage( int UserId, int MessageId ) {
            return _context.Messages.Include(c => c.MessageText)
                .Where( c => c.SenderId==UserId && c.MessageId==MessageId )
                .FirstOrDefault();
        }

        //Get All Messages -> UserID, MessageID
        public IEnumerable<MessageEntity> GetMessages( int UserId) {
            return _context.Messages.Where(c => c.ReceiverId==UserId)
                .ToList();
        }

        //Get User -> Using User ID
        public UserEntity GetUser( int userId ) {
            return _context.Users.Where(c => c.Id==userId).FirstOrDefault();
        }

        //Get All the users
        public IEnumerable<UserEntity> GetUsers() {
            return _context.Users.OrderBy(c => c.FirstName).ToList();
        }

        //Login User
        public UserEntity LoginUser(UserEntity User, string UIDA) {
            var nm = _context.Users.Where(c => c.Username.Equals(User.Username) && c.Password.Equals(User.Password) && c.IsActive.Equals(1))
                .FirstOrDefault();
            if (nm==null) {
                return User;
            }
                nm.ActiveCookie=UIDA;
                SaveChanges();
            return nm;
        }

        //Is user logged in
        public bool IsLoggedIn(int UserId, string UIDA) {
            return _context.Users.Any(c => c.Id.Equals(UserId)&&c.ActiveCookie.Equals(UIDA));
        }

        //Save Changes to the database
        public bool SaveChanges() {
            return _context.SaveChanges()>=0;
        }

        public bool UserExists( string Username ) {
            return _context.Users.Any(c => c.Username==Username);
        }

        public bool LogutUser( int userId) {
            var usr = _context.Users.Where(c => c.Id==userId).FirstOrDefault();
            if (usr!=null) {
                usr.ActiveCookie="";
                SaveChanges();
                return true;
            }
            return false;
        }

        //Send a message
        public bool SendMessage( int sender, int receiver, string messageText ) {
            var msg = _context.Messages.Add(new MessageEntity {
                SenderId=sender,
                ReceiverId= receiver,
                MessageText=messageText,
                Status=0,
                IsActive=1
            });
            return SaveChanges();
        }
    }
}
