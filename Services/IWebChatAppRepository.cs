﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebChat.Entities;

namespace WebChat.Services {
    public interface IWebChatAppRepository {
        IEnumerable<UserEntity> GetUsers();
        UserEntity GetUser( int userId );
        

        IEnumerable<MessageEntity> GetMessages( int UserId);
        MessageEntity GetMessage( int UserId, int MessageId );

        //Search for Username
        bool UserExists( string Username );

        //Add a user
        void AddUser(UserEntity User);

        //User Login
        UserEntity LoginUser(UserEntity User, string UIDA);

        //Is User Logged In
        bool IsLoggedIn( int UserID, string UIDA );

        //Logout User
        bool LogutUser( int userId);

        //Save All changes
        bool SaveChanges();

        //Send Message
        bool SendMessage( int sender, int receiver, string messageText );
        
    }
}
