﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebChat.Entities;
using WebChat.Services;

namespace WebChat {
    public class Startup {

        //public static IConfigurationRoot Configuration;

        public Startup( IHostingEnvironment env ) {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appSettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration=builder.Build();
        }

        public static IConfiguration Configuration { get; private set; }



        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices( IServiceCollection services ) {

            //Add MVC Service
            services.AddMvc().AddMvcOptions(o => o.OutputFormatters
                .Add(new XmlDataContractSerializerOutputFormatter()));

            //var connectionString = @"Server=(localdb)\MSSQLLocalDB;Database=CityInfoDB;Trusted_Connection=True;";
            //var connectionString = Startup.Configuration["connectionStrings:cityInfoDBConnectionString"];
            var connectionString = @"Server=(localdb)\ProjectsV13;Database=WebChatApp;Trusted_Connection=True";
            services.AddDbContext<WebChatAppContext>(o => o.UseSqlServer(connectionString));

            services.AddScoped<IWebChatAppRepository, WebChatAppRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IHostingEnvironment env ) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseStatusCodePages();

            //AutoMapper.Mapper.Initialize(cfg => {
            //    cfg.CreateMap<Entities.UserEntity, Models.UserModel>();
            //    cfg.CreateMap<Entities.MessageEntity, Models.MessageModel>();
            //});

            //using MVC
            app.UseMvc();

            
            //app.Run(async ( context ) => {
            //    await context.Response.WriteAsync("Hello World!");
            //});
        }
    }
}
