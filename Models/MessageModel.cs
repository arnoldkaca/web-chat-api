﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChat.Models {
    public class MessageModel {
        public int MessageId { get; set; }
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        public string MessageText { get; set; }
        public int Status { get; set; }
        public int IsActive { get; set; }
    }

    public class MessageUpdateModel {
        public int MessageId { get; set; }
        public int Status { get; set; }
    }
}
