﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebChat.Entities {
    public class MessageEntity {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MessageId { get; set; }
        [ForeignKey("SenderId")]
        public UserEntity UserId { get; set; }
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        [Required]
        [MaxLength(500)]
        public string MessageText { get; set; }
        public int Status { get; set; }
        public int IsActive { get; set; }
        
    }

}
