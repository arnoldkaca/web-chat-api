﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebChat.Models;

namespace WebChat.Entities {
    public class UserEntity {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(50)]
        public string Username { get; set; }
        public string Password { get; set; }
        public string Status { get; set; }
        public int RoleId { get; set; }
        public int IsActive { get; set; }
        public string ActiveCookie {get; set;}

        //public ICollection<MessageEntity> Messages { get; set; } = new List<MessageEntity>();
    }
}
